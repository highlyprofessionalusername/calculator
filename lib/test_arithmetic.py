import unittest
import calculator

class TestArithmeticMethods(unittest.TestCase):
    def test_add_positive_integer(self):
        result = calculator.addition(1, 3)
        self.assertEqual(result, 4)

    def test_subtract_positive_integer(self):
        result = calculator.subtraction(4, 3)
        self.assertEqual(result, 1)

    def test_multiply_positive_integer(self):
        result = calculator.multiplication(2, 2)
        self.assertEqual(result, 4)

    def test_divide_positive_integer(self):
        result = calculator.division(12, 4)
        self.assertEqual(result, 3)


if __name__ == '__main__':
    unittest.main()