stack = []
prompt = input("Enter calculation in postfix notation:" ) #Defines the prompt, and the text it presents
formula = prompt.split() #Split the prompt into discrete values, so they can go into the list
operators = ["+", "-", "*", "/"] #Defines the operators
## Tests whether something is an operator or not
def is_operator(var):
    if var in operators:
        return True
    else:
        return False

def addition(var1, var2):
    return int(var1) + int(var2)

def subtraction(var1, var2):
    return int(var1) - int(var2)

def division(var1, var2):
    return int(var1) / int(var2)

def multiplication(var1, var2):
    return int(var1) * int(var2)


for i in formula:
    if not is_operator(i):
        stack.append(i)
    elif stack:
        oper = i
        var2 = stack.pop()
        var1 = stack.pop() 
        if oper == "*":
            result = var1, "*", var2, "=", multiplication(var1, var2)
            print(result)
        elif oper == "/":
            result = var1, "/", var2, "=", division(var1, var2)
            print(result)
        elif oper == "+":
            result = var1, "+", var2, "=", addition(var1, var2)
            print(result)
        elif oper == "-":
            result = var1, "-", var2, "=", subtraction(var1, var2)
            print(result)
